SUMMARY
-------

This module lets you to exclude a set of assets (CSS and JavaScript) from the
active theme's `.info` file. The goal is to reduce the weight of resources sent
to the client to improve page load time, but also to facilitate the creation of
theme avoiding overloads CSS or JavaScript behaviors.

Each asset can be disabled for everyone or only for the a list of specified user
role(s).

REQUIREMENTS
------------

None.

INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.

USAGE
-----

After installing and enabling the module, you can specify assets to be removed
from the active theme:

1. Edit the `.info` file of your theme.
2. For each CSS that you want to exclude from the page, add a line starting
   with the keyword `unset`, like this:
   
       unset[stylesheets][all][] = theStylesheetToDisable.css
   
   Note that the word `all` is not a media property but a wildcard to specify 
   that the exclusion rule should be applied to all the users. You can specify
   existing roles. Eg, to disable the CSS only for anonymous users:
   
       unset[stylesheets][anonymous user][] = theStylesheetToDisable.css
   
   The roles may be combined, separated by commas:
   
       unset[stylesheets][role1, role2, role3][] = theStylesheetToDisable.css
3. Similarly, add a line for each JavaScript file to exclude, specifying the
   keyword `scripts`:
   
       unset[scripts][all][] = theJSToDisable.js
   
   Again, you can refine the exclusion specifying the user role(s):
   
       unset[scripts][role1, role2, role3][] = theJSToDisable.js
4. Clear all caches

ADVANCED USAGE
--------------

You can specify a generic file name that will be compatible with all
environments (development or production), whether the ressource will be minified
or not:

    unset[scripts][all][] = jquery.cycle/jquery.cycle.*.js

Also, if you want to unset all the JavaScript of jQuery UI, simply add the
following line:

    unset[scripts][all][] = jquery.ui/ui.*.js

See http://www.php.net/function.fnmatch.php for further information about the
pattern syntax allowed in the filename.

Note: I precise here the name of the module as a prefix to clarify my info file.

EXAMPLE
-------

Bellow, a sample `.info` file:

    ;--------------------- unset module styles ----------------
    unset[stylesheets][all][] = aggregator/aggregator.css
    unset[stylesheets][all][] = boxes/boxes.css
    unset[stylesheets][all][] = cck/content-module.css
    unset[stylesheets][all][] = cck/fieldgroup.css
    unset[stylesheets][all][] = date/date*.css
    unset[stylesheets][all][] = date/jquery.timeentry.css
    unset[stylesheets][all][] = filefield/filefield.css
    unset[stylesheets][all][] = jquery-ui/jquery-ui.css
    unset[stylesheets][all][] = node/node.css
    unset[stylesheets][all][] = panels/panels.css
    unset[stylesheets][all][] = system/defaults.css
    unset[stylesheets][all][] = system/system-menus.css
    unset[stylesheets][all][] = webform/webform.css
    unset[stylesheets][all][] = user/user.css
    unset[stylesheets][all][] = views/views.css
    unset[stylesheets][anonymous user][] = ctools/ctools.css
    unset[stylesheets][anonymous user][] = system/system.css

    ;--------------------- unset module scripts ---------------
    unset[scripts][all][] = panels/panels.js

FAQ
---

Q: Is this module compatible with the minification/aggregation of CSS and
   JavaScript?
A: Yes.

Q: Is this module compatible with the module "jQuery Update"?
A: Yes. Unset Assets is executed after all other modules or themes and doing
   its best to not break the existing stack of JavaScript files.

Q: Is there a list of compatibility with other modules?
A: Unset Assets is fully compatible with the following modules:
     - CDN
     - JavaScript Aggregator
     - jQuery Update
     - jQuery UI

TODO
----

* Be compatible with Advanced CSS/JS Aggregation.
* Remove the settings keys "cssInit" and "jsInit" added by the module
  Nodereference Explorer.

CONTACT
-------

Current maintainer:

* Mehdi Kabab (piouPiouM) – http://drupal.org/user/2110250
