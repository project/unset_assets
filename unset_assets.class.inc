<?php
/**
 * @file
 * Helper class for the Unset Assets module.
 */

/**
 * A simple helper class.
 */
class UnsetAssetsHelper {
  const WILDCARD_ROLE = 'all';
  const HOOK_POSTPROCESS_SUFFIX = '_unset_assets_postprocess';

  /**
   * Backup of the persistent variable 'preprocess_js'.
   *
   * @var boolean
   */
  public static $oldPreprocessJs = FALSE;

  /**
   * A counter of assets that have been unseted.
   *
   * @var int
   */
  public $unsetCounter = 0;

  /**
   * An array of CSS or JavaScript files for a given type (core, module, theme).
   *
   * @var array
   */
  protected $input;

  /**
   * A stack of normalized arrays with path and basename of the asset to test.
   *
   * @var array
   */
  protected $stack;

  /**
   * The user account object.
   *
   * @var object
   */
  protected $user;

  /**
   * Perform the logic.
   *
   * @param array $input
   *   An array of CSS or JavaScript files for a given type
   *   (core, module, theme).
   * @param array $filters
   *   An array of files to unset.
   * @param object $user
   *   The user account object.
   *
   * @return array
   *   The input without the assets set in the theme info.
   *
   * @see drupal_get_css()
   * @see drupal_get_js()
   */
  public function run($input, $filters, $user) {
    $stack = array_keys($input);
    array_walk($stack, array(__CLASS__, 'normalizeResource'));

    // We need to access to this variables from filterAsset().
    $this->input = &$input;
    $this->stack = &$stack;
    $this->user  = $user;

    foreach ($stack as $i => $asset_infos) {
      foreach ($filters as $role => $to_unset) {
        $this->filterAsset($to_unset, $asset_infos, $role, $i);
      }
    }

    return $input;
  }

  /**
   * Unload the given filemames to the assets stack.
   *
   * @param array $to_unset
   *   An array of filename to unset.
   * @param array $asset_infos
   *   An associative array with path and basename of the asset to test.
   * @param string $role
   *   A list of role name affected by the filter, separated by a comma.
   * @param int $i
   *   The index of the resource in UnsetAssetsHelper->stack tested.
   *
   * @link fnmatch() Used to Match filename against a pattern.
   */
  public function filterAsset($to_unset, $asset_infos, $role, $i) {
    $target_roles = array_map('trim', explode(',', $role));
    if (self::WILDCARD_ROLE === $role || in_array(self::WILDCARD_ROLE, $target_roles) || count(array_intersect($this->user->roles, $target_roles))) {
      foreach ($to_unset as $filename) {
        $basename = basename($filename);
        if ($basename === $asset_infos['basename'] || fnmatch($basename, $asset_infos['basename'])) {
          unset($this->input[$asset_infos['path']], $this->stack[$i]);
          ++$this->unsetCounter;
        }
      }
    }
  }

  /**
   * Tranforms the given path into an associative array.
   *
   * @param string &$item
   *   The path to normalize.
   */
  public static function normalizeResource(&$item) {
    $item = array('path' => $item, 'basename' => basename($item));
  }

  /**
   * Alter the theme registry information the given hook.
   *
   * If is found, the given hook is removed from the page preprocess registry.
   * By default it will be appended to the end of the registry, so it will be
   * fired at the end of every preprocess page.
   *
   * @param array &$registry
   *   The entire cache of theme registry information, post-processing.
   * @param string $name
   *   The hook name to alter.
   * @param bool $append
   *   Set to FALSE to not append the hook at the end of the registry.
   */
  public static function alterThemeRegistry(&$registry, $name, $append = TRUE) {
    $index = array_search($name, $registry['page']['preprocess functions'], TRUE);
    if (FALSE !== $index) {
      array_splice($registry['page']['preprocess functions'], $index, 1);
      if ($append) {
        $registry['page']['preprocess functions'][] = $name;
      }
    }
  }

  /**
   * Temporarily set the variable "preprocess_js" without any SQL query.
   *
   * @param bool $value
   *   The value to set.
   *
   * @see variable_get()
   */
  public static function setPreprocessJS($value) {
    global $conf;

    $conf['preprocess_js'] = (boolean) $value;
  }

  /**
   * Restores the variable "preprocess_js".
   */
  public static function restorePreprocessJS() {
    self::setPreprocessJS(self::$oldPreprocessJs);
  }
}

