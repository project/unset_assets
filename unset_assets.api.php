<?php
/**
 * @file
 * Hooks provided by the Unset Assets module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Process variables for page.tpl.php after Unset Assets action.
 *
 * Provides a way for the modules and the active theme to interact with the
 * template variables altered by the module Unset Assets.
 *
 * Warning: do not use the following functions:
 * - drupal_add_css()
 * - drupal_add_js()
 * - drupal_get_css()
 * - drupal_get_js()
 */
function hook_unset_assets_postprocess(&$variables) {
  // Remove attributes and closings tags become useless in HTML5.
  $variables['styles']  = str_ireplace(
    array('type="text/css" ', ' />'),
    array('', '>'),
    $variables['styles']
  );

  // Note: the following can be overridden by the module JavaScript Aggregator.
  $variables['scripts'] = str_ireplace(
    'type="text/javascript" ',
    '',
    $variables['scripts']
  );
}

/**
 * @} End of "addtogroup hooks".
 */
